package View;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import neural.TwoLayerPerceptron;

public class UseMenu extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7159060339459223693L;

	public JButton back = new JButton("COFNIJ");
	
	private GridLayout resultsLayout = new GridLayout(1, 2), mainLayout = new GridLayout(2, 1), teamsGrid = new GridLayout(1, 2), buttonsGrid = new GridLayout(4, 1), teamXGrid = new GridLayout(0, 1);
	private JPanel results = new JPanel(), teams = new JPanel(), buttons = new JPanel(), leftTeamPanel = new JPanel(), rightTeamPanel = new JPanel();
	private JButton startButton = new JButton("START"), fromFileButton = new JButton("WPROWADŹ PARAMETRY Z PLIKU");
	private JLabel[] labels1 = {
			new JLabel("ŁĄCZNA WARTOŚĆ PIŁKARZY"),
			new JLabel("LICZBA PIŁKARZY W KADRZE"),
			new JLabel("ŚREDNIA WARTOŚĆ PIŁKARZA"),
			new JLabel("ŚREDNI WIEK"),
			new JLabel("LICZBA OBCOKRAJOWCÓW"),
			new JLabel("MISTRZOSTWA + PUCHARY"),
			new JLabel("WARTOŚĆ NAJDROŻSZEGO PIŁKARZA"),
			new JLabel("PUNKTY/MECZE W POPRZEDNIM SEZONIE"),
			new JLabel("RÓŻNICA BRAMEK W POPRZEDNIM SEZONIE")
	};
	private JTextField[] txtFields1 = {
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField()
	};
	
	private JLabel[] labels2 = {
			new JLabel("ŁĄCZNA WARTOŚĆ PIŁKARZY"),
			new JLabel("LICZBA PIŁKARZY W KADRZE"),
			new JLabel("ŚREDNIA WARTOŚĆ PIŁKARZA"),
			new JLabel("ŚREDNI WIEK"),
			new JLabel("LICZBA OBCOKRAJOWCÓW"),
			new JLabel("MISTRZOSTWA + PUCHARY"),
			new JLabel("WARTOŚĆ NAJDROŻSZEGO PIŁKARZA"),
			new JLabel("PUNKTY/MECZE W POPRZEDNIM SEZONIE"),
			new JLabel("RÓŻNICA BRAMEK W POPRZEDNIM SEZONIE")
	};
	private JTextField[] txtFields2 = {
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField(),
			new JTextField()
	};
	
	private JLabel result = new JLabel("-");
	private JLabel resultLabel = new JLabel("WYNIK (WYJŚCIE SIECI): ");
	private JScrollPane leftTeam, rightTeam;
	
	public UseMenu(final TwoLayerPerceptron TLP) {

        leftTeam = new JScrollPane(leftTeamPanel);
        rightTeam = new JScrollPane(rightTeamPanel);
        
        resultLabel.setHorizontalAlignment(JLabel.RIGHT);
		result.setHorizontalAlignment(JLabel.LEFT);
		
		this.setLayout(mainLayout);
		teams.setLayout(teamsGrid);
		buttons.setLayout(buttonsGrid);
		leftTeamPanel.setLayout(teamXGrid);
		rightTeamPanel.setLayout(teamXGrid);
		results.setLayout(resultsLayout);
		
		for(int i = 0; i<labels1.length; i++){
			labels1[i].setHorizontalAlignment(JLabel.CENTER);
			labels2[i].setHorizontalAlignment(JLabel.CENTER);
			leftTeamPanel.add(labels1[i]);
			leftTeamPanel.add(txtFields1[i]);
			rightTeamPanel.add(labels2[i]);
			rightTeamPanel.add(txtFields2[i]);
		}
		
		teams.add(leftTeam);
		teams.add(rightTeam);
		
		results.add(resultLabel);
		results.add(result);
		buttons.add(results);
		buttons.add(startButton);
		buttons.add(fromFileButton);
		buttons.add(back);
		
		this.add(teams);
		this.add(buttons);
		
		 /*### import ###*/
        fromFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	JFileChooser jf = new JFileChooser();
            	jf.setDialogTitle("Podaj plik z zapisanymi wagami");
            	int r = jf.showOpenDialog(new JFrame());
            	if (r == JFileChooser.APPROVE_OPTION) {
            	  	File importFile = jf.getSelectedFile();
            	  	BufferedReader in;
    				try {
    					in = new BufferedReader(new FileReader(importFile));
    	            	for(String line = in.readLine(); line != null; line = in.readLine()){
    	            		String[] params = line.split(";");
    	            		for(int i=0; i<txtFields1.length; i++){
    	            			txtFields1[i].setText(params[i]);
    	            		}
    	            		for(int i=0; i<txtFields2.length; i++){
    	            			txtFields2[i].setText(params[i+9]);
    	            		}
    	            	}
    				} catch (IOException e1) {
    					JOptionPane.showMessageDialog(null, "Brak podanego pliku!");
    				}
            	}	
            }
        });
        
        /*### start ###*/
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	ArrayList<Double> parameters = new ArrayList<Double>();
            	for(int i=0; i<txtFields1.length; i++){
            		double param;
            		try {
            			String par = (txtFields1[i].getText()).replaceAll(",", ".");
						param = Double.parseDouble(par);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, "Niepoprawne parametry!");
						return;
					}
            		//#########################################
        			 /*TEAM A PARAMS*/
        			parameters.add(param);
        			//#########################################
        		}
            	parameters.add(0.0);
        		for(int i=0; i<txtFields2.length; i++){
        			double param;
            		try {
            			String par = (txtFields2[i].getText()).replaceAll(",", ".");
            			param = Double.parseDouble(par);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, "Niepoprawne parametry!");
						return;
					}
            		//#########################################
        			/*TEAM B PARAMS*/
            		parameters.add(param);
        			//#########################################
        		}
        		TLP.update(parameters);
        		result.setText(TLP.getOutputs().get(0).getValue().toString());
            }
        });
	}
	
}
