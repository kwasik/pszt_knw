package View;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import neural.TwoLayerPerceptron;

public class LearnMenu extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JButton back;
	private GridLayout gridLayout = new GridLayout(2, 0);
	
	private JPanel buttons1, coefficientPanel, buttons2;
	private JButton fromFile, fromKeyboard, deleteRow, learnIt;
	private JLabel rowsCount, learnCoefficientLabel, learnSpeedLabel;
	private JTable table;
	private JTextField learnCoefficientTxt, learnSpeedTxt;
	DefaultTableModel tableModel = new DefaultTableModel(0, 0);
	
	public LearnMenu(final TwoLayerPerceptron TLP) {
		rowsCount = new JLabel("0 rekordów");
		learnCoefficientLabel = new JLabel("Współczynnik uczenia (szybkość):");
		learnCoefficientLabel.setHorizontalAlignment(JLabel.CENTER);
		learnSpeedLabel = new JLabel("Czas uczenia:");
		learnSpeedLabel.setHorizontalAlignment(JLabel.CENTER);
		learnCoefficientTxt = new JTextField("0.01");
		learnCoefficientTxt.setHorizontalAlignment(JTextField.CENTER);
		learnSpeedTxt = new JTextField("20000");
		learnSpeedTxt.setHorizontalAlignment(JTextField.CENTER);
		rowsCount.setHorizontalAlignment(JLabel.CENTER);
		fromFile = new JButton("WPROWADŹ CIĄG UCZĄCY Z PLIKU");
		fromKeyboard = new JButton("DODAJ NOWY REKORD (PUSTY)");
		deleteRow = new JButton("USUŃ ZAZNACZONE");
		learnIt = new JButton("UCZ SIEĆ");
		back = new JButton("COFNIJ");
		
		buttons1 = new JPanel();
		coefficientPanel = new JPanel();
		buttons2 = new JPanel();
		
		this.setLayout(gridLayout);
		buttons1.setLayout(new GridLayout(6, 1));
		coefficientPanel.setLayout(new GridLayout(2, 2));
		buttons2.setLayout(new GridLayout(1, 2));

		buttons1.add(rowsCount);
		coefficientPanel.add(learnCoefficientLabel);
		coefficientPanel.add(learnSpeedLabel);
		coefficientPanel.add(learnCoefficientTxt);
		coefficientPanel.add(learnSpeedTxt);
		buttons1.add(coefficientPanel);
		buttons1.add(fromFile);
		buttons2.add(fromKeyboard);
		buttons2.add(deleteRow);
		buttons1.add(buttons2);
		buttons1.add(learnIt);
		buttons1.add(back);
		
		String[] columnNames = {
				"ŁĄCZNA WARTOŚĆ",
				"KADRA",
				"ŚREDNIA WARTOŚĆ",
				"WIEK",
				"OBCOKRAJOWCY",
				"MISTRZOSTWA",
				"NAJDROŻSZY PIŁKARZ",
				"PUNKTY NA MECZ",
				"BRAMKI",
				"BOISKO",
				"ŁĄCZNA WARTOŚĆ",
				"KADRA",
				"ŚREDNIA WARTOŚĆ",
				"WIEK",
				"OBCOKRAJOWCY",
				"MISTRZOSTWA",
				"NAJDROŻSZY PIŁKARZ",
				"PUNKTY NA MECZ",
				"BRAMKI",
				"WYNIK"
		};

		// create object of table and table model
		table = new JTable();
		tableModel = new DefaultTableModel(0, 0);
		tableModel.setColumnIdentifiers(columnNames);
		table.setModel(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		class ThickRightBorderCellRenderer extends DefaultTableCellRenderer {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Border getBorder() {
				return BorderFactory.createMatteBorder(0, 0, 0, 2, Color.RED);
			}
		}
		table.getColumnModel().getColumn(9).setCellRenderer(
				new ThickRightBorderCellRenderer()
		);
		table.getColumnModel().getColumn(8).setCellRenderer(
				new ThickRightBorderCellRenderer()
		);
		table.getColumnModel().getColumn(18).setCellRenderer(
				new ThickRightBorderCellRenderer()
		);
		
		this.add(scrollPane);
		this.add(buttons1); 
	
		/*### import data from file ###*/
        fromFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	JFileChooser jf = new JFileChooser();
            	jf.setDialogTitle("Podaj plik z danymi");
            	int r = jf.showOpenDialog(new JFrame());
            	if (r == JFileChooser.APPROVE_OPTION) {
            	  	File importFile = jf.getSelectedFile();
            	  	BufferedReader in;
    				try {
    					in = new BufferedReader(new FileReader(importFile));
    	            	for(String line = in.readLine(); line != null; line = in.readLine()){
    	            		tableModel.addRow(line.split(";"));
    	            	}
    				} catch (IOException e1) {
    					JOptionPane.showMessageDialog(null, "Brak podanego pliku!");
    				}
            	}	
            	rowsCount.setText(((Integer)table.getRowCount()).toString() + " rekordów");
            }
        });
        
        /*### get data from keyboard (add empty row) ###*/
        fromKeyboard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	tableModel.addRow(new Object[] {});
            	table.changeSelection(table.getRowCount() - 1, 0, false, false);
            	rowsCount.setText(((Integer)table.getRowCount()).toString() + " rekordów");
            }
        });
        
        /*### delete selected rows ###*/
        deleteRow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	int[] rows = table.getSelectedRows();
            	   for(int i=0;i<rows.length;i++){
            	     tableModel.removeRow(rows[i]-i);
            	 }
            	 rowsCount.setText(((Integer)table.getRowCount()).toString() + " rekordów");
            }
        });
        
        /*### learn! ###*/
        learnIt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	if(tableModel.getRowCount() == 0){
            		JOptionPane.showMessageDialog(null, "Brak danych!");
            		return;
            	}
            	final ArrayList<ArrayList<Double>> allRows = new ArrayList<ArrayList<Double>>();
                for (int i = 0; i < tableModel.getRowCount(); i++) {
                	ArrayList<Double> oneRow = new ArrayList<Double>();
                	for(int j = 0; j < tableModel.getColumnCount(); j++){
                		double param;
                		try {
                			String par = ((String)tableModel.getValueAt(i, j)).replaceAll(",", ".");
    						param = Double.parseDouble(par);
    						oneRow.add(param);
    					} catch (Exception e1) {
    						JOptionPane.showMessageDialog(null, "Niepoprawne parametry!");
    						return;
    					}
                	}
                	allRows.add(oneRow);
                }
                /*###*/
                final Double parametr1 = Double.parseDouble(learnCoefficientTxt.getText());
                final long parametr2 = Long.parseLong(learnSpeedTxt.getText());
                new Thread(new Runnable() {
                    public void run() {
                    	back.setBackground(Color.gray);
                    	fromFile.setBackground(Color.gray);
                    	fromKeyboard.setBackground(Color.gray);
                    	learnIt.setBackground(Color.gray);
                    	deleteRow.setBackground(Color.gray);
                    	back.setEnabled(false);
                    	fromFile.setEnabled(false);
                    	fromKeyboard.setEnabled(false);
                    	learnIt.setEnabled(false);
                    	deleteRow.setEnabled(false);
                    	
                        TLP.learn(allRows, parametr2, parametr1);
                        
                        back.setBackground(new JButton().getBackground());
                    	fromFile.setBackground(new JButton().getBackground());
                    	fromKeyboard.setBackground(new JButton().getBackground());
                    	learnIt.setBackground(new JButton().getBackground());
                    	deleteRow.setBackground(new JButton().getBackground());
                    	back.setEnabled(true);
                    	fromFile.setEnabled(true);
                    	fromKeyboard.setEnabled(true);
                    	learnIt.setEnabled(true);
                    	deleteRow.setEnabled(true);
                    }
                }).start();
                /*###*/
            }
        });
        
        
	}
	
}
