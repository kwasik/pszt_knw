package View;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import neural.TwoLayerPerceptron;

public class MainMenu extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 547321670375376592L;

	public JButton learnMenuButton, useMenuButton;

	private JLabel windowTitle1, windowTitle2, authors;
	private JButton exitButton;
	private GridLayout gridLayout = new GridLayout(0, 2);
	
	public MainMenu(TwoLayerPerceptron TLP) {
		setLayout(gridLayout);
		windowTitle1 = new JLabel("MENU ");
		windowTitle2 = new JLabel("GŁÓWNE");
		windowTitle1.setHorizontalAlignment(JLabel.RIGHT);
		windowTitle1.setFont(new Font(windowTitle1.getName(), Font.BOLD, 40));
		windowTitle2.setHorizontalAlignment(JLabel.LEFT);
		windowTitle2.setFont(new Font(windowTitle2.getName(), Font.BOLD, 40));
		authors = new JLabel("<html>Krystian Wąsik<br>Marcin Nowicki<br>Paweł Koszelew<br>© 2015</html>");
		authors.setHorizontalAlignment(JLabel.LEFT);
		authors.setVerticalAlignment(JLabel.BOTTOM);
		authors.setFont(new Font(authors.getName(), Font.BOLD, 10));
		learnMenuButton = new JButton("UCZENIE SIECI");
		useMenuButton = new JButton("SIEĆ NAUCZONA");
		exitButton = new JButton("ZAKOŃCZ");
		
		this.add(windowTitle1);
		this.add(windowTitle2);
        this.add(learnMenuButton);
        this.add(useMenuButton);
        this.add(authors);
        this.add(exitButton);
        
        /*### exit ###*/
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });      
	}
	
}
