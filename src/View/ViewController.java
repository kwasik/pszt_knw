package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import neural.TwoLayerPerceptron;

public class ViewController {

	private MainMenu mainMenu;
	private LearnMenu learnMenu;
	private UseMenu useMenu;
	private JFrame frame;
	
	public ViewController(JFrame fr, TwoLayerPerceptron TLP) {
		this.frame = fr;
		mainMenu = new MainMenu(TLP);
		frame.add(mainMenu);
		learnMenu = new LearnMenu(TLP);
		useMenu = new UseMenu(TLP);
		
		mainMenu.learnMenuButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                frame.add(learnMenu);
                mainMenu.setVisible(false);
                frame.remove(mainMenu);
                
            }
        });    
		mainMenu.useMenuButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	frame.add(useMenu);
                mainMenu.setVisible(false);
                frame.remove(mainMenu);
            }
        });
		learnMenu.back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	frame.remove(learnMenu);
            	frame.add(mainMenu);
            	mainMenu.setVisible(true);
                
            }
        });
		useMenu.back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                frame.remove(useMenu);
                frame.add(mainMenu);
            	mainMenu.setVisible(true);            
            }
        });
	}
}
