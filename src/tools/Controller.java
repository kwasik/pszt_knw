package tools;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;

import View.GUIConsole;
import View.ViewController;

import neural.*;



public class Controller {
	static ArrayList<Connectable> parameters;
	static TwoLayerPerceptron TLP;
	public Controller(){
		
	}
	
	public static void main(String[] args) {
		
		
		parameters= new ArrayList<Connectable>();
		
		TLP = new TwoLayerPerceptron(19, 200, 1);
		
		parameters = TLP.getInputs();
		
		JFrame frame = new JFrame ("Sieć neuronowa");
		new ViewController(frame, TLP);
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setVisible (true);
		frame.setMinimumSize(new Dimension(600, 400));
		
		new GUIConsole();
	}
}
