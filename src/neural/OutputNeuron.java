package neural;

import java.util.ArrayList;
import java.util.Iterator;

public class OutputNeuron extends Neuron {

	public OutputNeuron() {
		super();
	}
	
	public OutputNeuron(double initialValue) {
		super(initialValue);
	}
	
	public OutputNeuron(ArrayList<Connectable> initialInputs) {
		super(initialInputs);
	}
	
	public OutputNeuron(ArrayList<Connectable> initialInputs, double initialValue) {
		super(initialInputs, initialValue);
	}
//	
//	protected double activationFunction(double x) {
//
//		return x;
//	}
//	
//	protected double derivativeOfActivationFunction(double x) {
//		
//		return 1;
//	}
	
	protected double activationFunction(double x) {
		
		return Math.pow(Math.E, x)/(1 + Math.pow(Math.E, x));
	}

	protected double derivativeOfActivationFunction(double x) {
		
		return Math.pow(Math.E, x)/Math.pow((1 + Math.pow(Math.E, x)), 2);
	}
	
	public double calculateErrorCoefficient(double rightOutput) {
		errorCoefficient = derivativeOfActivationFunction(weightedSumOfInputs) * (rightOutput - value);
		
		return errorCoefficient;
	}
	
	public void propagateErrorCoefficient() {
		Connection currentConnection;
		
		
		for(Iterator<Connection> it = connections.iterator(); it.hasNext();) {
			currentConnection = it.next();
			
			if(currentConnection.getFrom() instanceof HiddenNeuron) {
				HiddenNeuron currentNeuron = (HiddenNeuron) currentConnection.getFrom();
				
				currentNeuron.propagateErrorCoefficient(errorCoefficient * getConnectionsWeight(currentNeuron));
			}
		}
	}
}
