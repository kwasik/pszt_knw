package neural;

public class OneConstParam extends ConstParam {
	
	private static OneConstParam instance = null;
	
	private OneConstParam() {
		super(1.0);
	}
	
	public static OneConstParam getInstance() {
		
		if(instance == null) {
			instance = new OneConstParam();
		}
		
		return instance;
	}
}
