package neural;

import java.util.ArrayList;
import java.util.Iterator;

public class TwoLayerPerceptron extends Perceptron {
	ArrayList<Connectable> inputs;
	ArrayList<HiddenNeuron> hiddenLayer;
	ArrayList<OutputNeuron> outputLayer;
	
	public TwoLayerPerceptron(int numOfInputs, int numOfHiddenNeurons, int numOfOutputNeurons) {
		inputs = new ArrayList<Connectable>();
		
		for(int i = 0; i < numOfInputs; i++) {
			inputs.add(new Param());
		}
		
		buildHiddenLayer(numOfHiddenNeurons);
		buildOutputLayer(numOfOutputNeurons);
	}
	
	public TwoLayerPerceptron(ArrayList<Connectable> initialInputs, int numOfHiddenNeurons, int numOfOutputNeurons) {
		inputs = new ArrayList<Connectable>(initialInputs);
		
		buildHiddenLayer(numOfHiddenNeurons);
		buildOutputLayer(numOfOutputNeurons);
	}
	
	protected void buildHiddenLayer(int numOfHiddenNeurons) {
		HiddenNeuron current;
		ArrayList<Connectable> hiddenLayerInputs;
		
		// Hidden layer's inputs consist of network's inputs and constant parameter (threshold)
		hiddenLayerInputs = new ArrayList<Connectable>(inputs);
		hiddenLayerInputs.add(OneConstParam.getInstance());
		
		hiddenLayer = new ArrayList<HiddenNeuron>();
		for(int i = 0; i < numOfHiddenNeurons; i++) {
			
			// Create new hidden neuron and add it to the hidden layer
			current = new HiddenNeuron(hiddenLayerInputs);
			hiddenLayer.add(current);
		}
	}
	
	protected void buildOutputLayer(int numOfOutputNeurons) {
		OutputNeuron current;
		ArrayList<Connectable> outputLayerInputs;
		
		// Output layer's inputs consist of hidden layer's outputs and constant parameter (threshold)
		outputLayerInputs = new ArrayList<Connectable>(hiddenLayer);
		outputLayerInputs.add(OneConstParam.getInstance());
		
		outputLayer = new ArrayList<OutputNeuron>();
		for(int i = 0; i < numOfOutputNeurons; i++) {
			
			// Create new output neuron and add it to the output layer
			current = new OutputNeuron(outputLayerInputs);
			outputLayer.add(current);
		}
	}
	
	public ArrayList<Connectable> getInputs() {
		return inputs;
	}
	
	public ArrayList<? extends Connectable> getOutputs() {
		return outputLayer;
	}
	
	public void setHiddenLayersWeights(ArrayList<ArrayList<Double>> weights) {
		if(weights.size() == hiddenLayer.size()) {
			for(int i = 0; i < weights.size(); i++) {
				hiddenLayer.get(i).setConnectionsWeight(weights.get(i));
			}
		} else {
			// TODO: Wrong size. Throw exception.
		}
	}
	
	public void setOutputLayersWeights(ArrayList<ArrayList<Double>> weights) {
		if(weights.size() == outputLayer.size()) {
			for(int i = 0; i < weights.size(); i++) {
				outputLayer.get(i).setConnectionsWeight(weights.get(i));
			}
		} else {
			// TODO: Wrong size. Throw exception.
		}
	}
	
	public ArrayList<ArrayList<Double>> getHiddenLayersWeights() {
		ArrayList<ArrayList<Double>> weights = new ArrayList<ArrayList<Double>>();
		
		for(int i = 0; i < hiddenLayer.size(); i++) {
			weights.add(hiddenLayer.get(i).getConnectionsWeight());
		}
		
		return weights;
	}
	
	public ArrayList<ArrayList<Double>> getOutputLayersWeights() {
		ArrayList<ArrayList<Double>> weights = new ArrayList<ArrayList<Double>>();
		
		for(int i = 0; i < outputLayer.size(); i++) {
			weights.add(outputLayer.get(i).getConnectionsWeight());
		}
		
		return weights;
	}

	public void update() {
		updateLayer(hiddenLayer);
		updateLayer(outputLayer);
	}
	
	public void update(ArrayList<Double> data){
		
		Param current;
		for(int i=0; i<data.size(); i++){
			if(inputs.get(i) instanceof Param) {
				current = (Param) inputs.get(i);
				current.setValue(data.get(i));
			}
		}
		
		update();
	}	
	
	public double learn(ArrayList<Double> rightOutputs, double learningCoefficient) {
		OutputNeuron current;
		double averageError=0;
		
		update();
		
		for(int i = 0; i < outputLayer.size(); i++) {
			current = outputLayer.get(i);
			current.calculateErrorCoefficient(rightOutputs.get(i));
			current.propagateErrorCoefficient();
			current.updateWeights(learningCoefficient);
		}
		
		for(Iterator<HiddenNeuron> it = hiddenLayer.iterator(); it.hasNext();) {
			it.next().updateWeights(learningCoefficient);
		}
		
		update();
		
		for(int i = 0; i < outputLayer.size(); i++) {
			current = outputLayer.get(i);
			averageError += Math.abs(current.calculateErrorCoefficient(rightOutputs.get(i)));
		}
		
		return averageError/outputLayer.size();
	}
	
	public void learn(ArrayList<ArrayList<Double>> data, long timeout, double learningCoefficient){
		
		double AvgCurrentError=999;
		double AvgPrevError;
		long start = System.currentTimeMillis();
		Param current;
		ArrayList<Double> rightOutputs = new ArrayList<Double>();
		rightOutputs.add(0.0);
		do{
			AvgPrevError=AvgCurrentError;
			AvgCurrentError=0;
			for(int i=0; i<data.size(); i++) {
				for(int j=0; j<data.get(i).size()-1; j++) {
					
					if(inputs.get(j) instanceof Param) {
						current = (Param) inputs.get(j);
						current.setValue(data.get(i).get(j));
					}
				}
				rightOutputs.set(0, data.get(i).get(19));
				AvgCurrentError+= learn(rightOutputs, learningCoefficient);
				
			}
			AvgCurrentError/=data.size();
			
			System.out.println("learn(): AvgCurrentError: " + AvgCurrentError);
			System.out.println("learn(): AvgPrevError: " + AvgPrevError);
		}
		while((start + timeout) > System.currentTimeMillis());	
		
		System.out.println("Nauka skonczona");
	}
}