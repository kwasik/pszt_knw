package neural;

public abstract class Connectable {
	protected double value;
	
	public Connectable() {
		value = 0;
	}
	
	public Connectable(double initialValue) {
		value = initialValue;
	}
	
	public Double getValue() {
		return new Double(value);
	}
}
