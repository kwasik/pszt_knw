package neural;

import java.lang.Math;
import java.util.ArrayList;

public class HiddenNeuron extends Neuron {

	public HiddenNeuron() {
		super();
	}
	
	public HiddenNeuron(double initialValue) {
		super(initialValue);
	}
	
	public HiddenNeuron(ArrayList<Connectable> initialInputs) {
		super(initialInputs);
	}
	
	public HiddenNeuron(ArrayList<Connectable> initialInputs, double initialValue) {
		super(initialInputs, initialValue);
	}

	protected double activationFunction(double x) {
		
		return Math.pow(Math.E, x)/(1 + Math.pow(Math.E, x));
	}

	protected double derivativeOfActivationFunction(double x) {
		
		return Math.pow(Math.E, x)/Math.pow((1 + Math.pow(Math.E, x)), 2);
	}
	
	public void propagateErrorCoefficient(double errorCoefficient) {
		this.errorCoefficient += errorCoefficient * derivativeOfActivationFunction(weightedSumOfInputs);
	}
}
