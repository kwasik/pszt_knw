package neural;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Perceptron {
	public abstract void update();

	protected void updateLayer(ArrayList<? extends Neuron> layer) {
		for(Iterator<? extends Neuron> it = layer.iterator(); it.hasNext();) {
			it.next().update();
		}
	}
}
