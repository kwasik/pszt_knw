package neural;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Neuron extends Connectable {
	
	ArrayList<Connection> connections;
	double weightedSumOfInputs;
	double errorCoefficient;
	
	protected abstract double activationFunction(double x);
	protected abstract double derivativeOfActivationFunction(double x);
	
	public Neuron() {
		super();
		connections = new ArrayList<Connection>();
	}
	
	public Neuron(double initialValue) {
		super(initialValue);
		connections = new ArrayList<Connection>();
	}
	
	public Neuron(ArrayList<Connectable> initialInputs) {
		super();
		
		connections = new ArrayList<Connection>();
		addInput(initialInputs);
	}
	
	public Neuron(ArrayList<Connectable> initialInputs, double initialValue) {
		super(initialValue);
		
		connections = new ArrayList<Connection>();
		addInput(initialInputs);
	}
	
	public void addInput(Connectable input) {
		Connection newConnection = new Connection(input);
		
		// TODO: Check if connection already exists
		
		connections.add(newConnection);
	}
	
	public void addInput(ArrayList<Connectable> inputs) {
		for(Iterator<Connectable> it = inputs.iterator(); it.hasNext();) {
			addInput(it.next());
		}
	}
	
	public void deleteInput(Connection connection) {		
		connections.remove(connection);
	}
	
	protected Connection findConnection(Connectable from) {
		Connection current;
		
		for(Iterator<Connection> it = connections.iterator(); it.hasNext();) {
			current = it.next();
			
			if(current.getFrom() == from) return current;
		}

		// TODO: Throw exception (NoConnection or something like that)
		return null;
	}
	
	public void setConnectionsWeight(Connectable from, double newWeight) {
		Connection connection;
		
		connection = findConnection(from);
		connection.setWeight(newWeight);
	}
	
	public void setConnectionsWeight(ArrayList<Double> weights) {
		if(weights.size() == connections.size()) {
			for(int i = 0; i < weights.size(); i++) {
				connections.get(i).setWeight(weights.get(i));
			}
		} else {
			// TODO: Wrong size. Throw exception.
		}
	}
	
	public double getConnectionsWeight(Connectable from) {
		Connection connection;
		
		connection = findConnection(from);
		return connection.getWeight();
	}
	
	public ArrayList<Double> getConnectionsWeight() {
		ArrayList<Double> weights;
		weights = new ArrayList<Double>();
		
		for(int i = 0; i < connections.size(); i++) {
			weights.add(connections.get(i).getWeight());
		}
		
		return weights;
	}
	
	public void update() {
		double sum = 0, weightedOutput;
		
		// TODO: What if there is no connections?
		
		for(Iterator<Connection> it = connections.iterator(); it.hasNext();) {
			weightedOutput = it.next().getWeightedOutput();
			sum += weightedOutput;
		}
		
		weightedSumOfInputs = sum;
		
		value = activationFunction(sum);
		
		errorCoefficient = 0;
	}
	
	public void updateWeights(double learningCoefficient) {
		double newWeight;
		Connection current;
		
		for(Iterator<Connection> it = connections.iterator(); it.hasNext();) {
			current = it.next();
			
			newWeight = current.getWeight() + learningCoefficient * errorCoefficient * current.getOutput();

			current.setWeight(newWeight);
		}
	}
}
