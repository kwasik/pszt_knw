package neural;

import java.util.Random;

public class Connection {
	private Connectable from;
	private double weight;
	
	public Connection(Connectable connFrom) {
		Random rnd = new Random();
		rnd.setSeed(System.currentTimeMillis());
		
		from = connFrom;
		weight = (float)rnd.nextLong()/Long.MAX_VALUE/1000;
		
		if(Double.isNaN(weight)) {
			System.out.println("wtf");
		}
		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Connection(Connectable connFrom, double intialWeight) {
		from = connFrom;
		weight = intialWeight;
	}
	
	public void setWeight(double newWeight) {
		
		if(Double.isNaN(newWeight)) {
			System.out.println("wtf");
		}
		
		weight = newWeight;
	}
	
	public double getWeight() {
		if(Double.isNaN(weight)) {
			System.out.println("wtf");
		}
		
		return weight;
	}
	
	public double getOutput() {
		return from.getValue();
	}
	
	public double getWeightedOutput() {
		if(Double.isNaN(weight)) {
			System.out.println("wtf");
		}
		
		return weight * from.getValue();
	}
	
	public Connectable getFrom() {
		return from;
	}
}
