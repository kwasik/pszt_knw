package neural;

public class Param extends Connectable {
	
	public Param() {
		super();
	}
	
	public Param(double initialValue) {
		super(initialValue);
	}
	
	public void setValue(double newValue) {
		value = newValue;
	}
}
